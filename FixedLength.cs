using Microsoft.ML.Data;

namespace MlSentimentOnnx;

public class FixedLength
{
    /// <summary>
    /// This is a fixed length vector designated by VectorType attribute.
    /// </summary>
    [VectorType(Config.FeatureLength)]
    public int[] Features { get; set; }
}