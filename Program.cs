﻿using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Transforms;

namespace MlSentimentOnnx;

static class Program
{
    static void Main(string[] args)
    {
        var modelPath = Path.Combine(Environment.CurrentDirectory, "SentimentModel");
        
        var mlContext = new MLContext();
        var lookupMap = mlContext.Data.LoadFromTextFile(Path.Combine(modelPath, "imdb_word_index.csv"),
            columns: new[]
            {
                new TextLoader.Column("Words", DataKind.String, 0),
                new TextLoader.Column("Ids", DataKind.Int32, 1),
            },
            separatorChar: ','
        );
        
        Action<VariableLength, FixedLength> ResizeFeaturesAction = (s, f) =>
        {
            var features = s.VariableLengthFeatures;
            Array.Resize(ref features, Config.FeatureLength);
            f.Features = features;
        };
        
        var tensorFlowModel = mlContext.Model.LoadTensorFlowModel(modelPath);
        
        Console.WriteLine("Hello, World!");
    }
    

}
