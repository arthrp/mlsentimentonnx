using Microsoft.ML.Data;

namespace MlSentimentOnnx;

public class MovieReviewSentimentPrediction
{
    [VectorType(2)]
    public float[] Prediction { get; set; }
}